﻿using System;

namespace QuanLyHocSinh.Model
{
    public abstract class BaseModel
    {
        public DateTime? NgayTao { get; set; }
        public string NguoiTao { get; set; }
        public string NguoiChinhSua { get; set; }
        public DateTime NgayChinhSua { get; set; }
        public short TinhTrang { get; set; }
    }
}
