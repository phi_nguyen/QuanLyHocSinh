﻿using System;

namespace QuanLyHocSinh.Model
{
    public class DangNhapModel : BaseModel
    {
        public string TenDangNhap { get; set; }
        public string MatKhau { get; set; }
        public string TenHienThi { get; set; }
        public DateTime? ThoiGianDangNhapGanNhat { get; set; }
    }
}
