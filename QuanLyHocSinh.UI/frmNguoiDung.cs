﻿using System.Windows.Forms;
using QuanLyHocSinh.BUS;
using QuanLyHocSinh.Model;

namespace QuanLyHocSinh.UI
{
    public partial class frmNguoiDung : BaseForm
    {
        private readonly DangNhapBus _bus = new DangNhapBus();
        public frmNguoiDung()
        {
            InitializeComponent();
        }

        protected override void HienThi()
        {
            dgvShowData.DataSource = _bus.GetData(string.Empty);
        }

        protected override void Lock()
        {
            TxtTenDangNhap.Enabled = false;
            txtTenHienThi.Enabled = false;
            TxtMatKhau.Enabled = false;
            base.Lock();
        }

        protected override void UnLock()
        {
            TxtTenDangNhap.Enabled = true;
            txtTenHienThi.Enabled = true;
            TxtMatKhau.Enabled = true;
            base.UnLock();
        }

        protected override void Clear()
        {
            TxtTenDangNhap.Text = string.Empty;
            txtTenHienThi.Text = string.Empty;
            TxtMatKhau.Text = string.Empty;
        }

        protected override void Sua()
        {
            base.Sua();
            TxtTenDangNhap.Enabled = false;
        }

        protected override void Luu()
        {
            var obj = new DangNhapModel
            {
                TenDangNhap = TxtTenDangNhap.Text.Trim(),
                MatKhau = TxtMatKhau.Text.Trim(),
                TenHienThi = txtTenHienThi.Text.Trim()
            };

            if (IsInsert == true)
            {
                //Kiem tra ten dang nhap
                var CheckUserResult = _bus.KiemTraTenDangNhap(TxtTenDangNhap.Text.Trim());
                if (CheckUserResult)
                {
                    MessageBox.Show("Tên đăng nhập đã tồn tại", "Thông báo", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    TxtTenDangNhap.Focus();
                    return;
                }

                _bus.Insert(obj);
            }
            else
            {
                _bus.Update(obj);
            }
            base.Luu();
        }

        private void dgvShowData_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                Lock();
                TxtTenDangNhap.Text = dgvShowData.Rows[e.RowIndex].Cells[0].Value.ToString();
                TxtMatKhau.Text = dgvShowData.Rows[e.RowIndex].Cells[1].Value.ToString();
                txtTenHienThi.Text = dgvShowData.Rows[e.RowIndex].Cells[2].Value.ToString();
            }
        }
    }
}
