﻿namespace QuanLyHocSinh.UI
{
    partial class BaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.BtnSua = new System.Windows.Forms.Button();
            this.BtnTaiLai = new System.Windows.Forms.Button();
            this.BtnXoa = new System.Windows.Forms.Button();
            this.BtnLuu = new System.Windows.Forms.Button();
            this.BtnTaoMoi = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.BtnSua);
            this.panel1.Controls.Add(this.BtnTaiLai);
            this.panel1.Controls.Add(this.BtnXoa);
            this.panel1.Controls.Add(this.BtnLuu);
            this.panel1.Controls.Add(this.BtnTaoMoi);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 536);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(793, 31);
            this.panel1.TabIndex = 0;
            // 
            // BtnSua
            // 
            this.BtnSua.Dock = System.Windows.Forms.DockStyle.Left;
            this.BtnSua.Location = new System.Drawing.Point(400, 0);
            this.BtnSua.Name = "BtnSua";
            this.BtnSua.Size = new System.Drawing.Size(75, 31);
            this.BtnSua.TabIndex = 4;
            this.BtnSua.Text = "Sửa";
            this.BtnSua.UseVisualStyleBackColor = true;
            this.BtnSua.Click += new System.EventHandler(this.BtnSua_Click);
            // 
            // BtnTaiLai
            // 
            this.BtnTaiLai.Dock = System.Windows.Forms.DockStyle.Left;
            this.BtnTaiLai.Location = new System.Drawing.Point(300, 0);
            this.BtnTaiLai.Margin = new System.Windows.Forms.Padding(4);
            this.BtnTaiLai.Name = "BtnTaiLai";
            this.BtnTaiLai.Size = new System.Drawing.Size(100, 31);
            this.BtnTaiLai.TabIndex = 3;
            this.BtnTaiLai.Text = "Tải lại";
            this.BtnTaiLai.UseVisualStyleBackColor = true;
            this.BtnTaiLai.Click += new System.EventHandler(this.BtnTaiLai_Click);
            // 
            // BtnXoa
            // 
            this.BtnXoa.Dock = System.Windows.Forms.DockStyle.Left;
            this.BtnXoa.Location = new System.Drawing.Point(200, 0);
            this.BtnXoa.Margin = new System.Windows.Forms.Padding(4);
            this.BtnXoa.Name = "BtnXoa";
            this.BtnXoa.Size = new System.Drawing.Size(100, 31);
            this.BtnXoa.TabIndex = 2;
            this.BtnXoa.Text = "Xóa";
            this.BtnXoa.UseVisualStyleBackColor = true;
            this.BtnXoa.Click += new System.EventHandler(this.BtnXoa_Click);
            // 
            // BtnLuu
            // 
            this.BtnLuu.Dock = System.Windows.Forms.DockStyle.Left;
            this.BtnLuu.Location = new System.Drawing.Point(100, 0);
            this.BtnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.BtnLuu.Name = "BtnLuu";
            this.BtnLuu.Size = new System.Drawing.Size(100, 31);
            this.BtnLuu.TabIndex = 1;
            this.BtnLuu.Text = "Lưu";
            this.BtnLuu.UseVisualStyleBackColor = true;
            this.BtnLuu.Click += new System.EventHandler(this.BtnLuu_Click);
            // 
            // BtnTaoMoi
            // 
            this.BtnTaoMoi.Dock = System.Windows.Forms.DockStyle.Left;
            this.BtnTaoMoi.Location = new System.Drawing.Point(0, 0);
            this.BtnTaoMoi.Margin = new System.Windows.Forms.Padding(4);
            this.BtnTaoMoi.Name = "BtnTaoMoi";
            this.BtnTaoMoi.Size = new System.Drawing.Size(100, 31);
            this.BtnTaoMoi.TabIndex = 0;
            this.BtnTaoMoi.Text = "Tạo mới";
            this.BtnTaoMoi.UseVisualStyleBackColor = true;
            this.BtnTaoMoi.Click += new System.EventHandler(this.BtnTaoMoi_Click);
            // 
            // BaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(793, 567);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "BaseForm";
            this.Text = "BaseForm";
            this.Load += new System.EventHandler(this.BaseForm_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button BtnTaiLai;
        private System.Windows.Forms.Button BtnXoa;
        private System.Windows.Forms.Button BtnLuu;
        private System.Windows.Forms.Button BtnTaoMoi;
        private System.Windows.Forms.Button BtnSua;
    }
}