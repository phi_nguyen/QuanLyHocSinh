﻿using System;
using System.Windows.Forms;
using QuanLyHocSinh.BUS;

namespace QuanLyHocSinh.UI
{
    public partial class frmDangNhap : Form
    {
        private readonly DangNhapBus _bus = new DangNhapBus();

        public frmDangNhap()
        {
            InitializeComponent();
        }

        private void frmDangNhap_Load(object sender, EventArgs e)
        {

        }

        private void BtnThoat_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void frmDangNhap_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void BtnDangNhap_Click(object sender, EventArgs e)
        {
            var tenDangNhap = TxtTenDangNhap.Text.Trim();
            var matKhau = TxtMatKhau.Text.Trim();

            if (string.IsNullOrWhiteSpace(tenDangNhap))
            {
                TxtTenDangNhap.Focus();
                return;
            }

            if (string.IsNullOrWhiteSpace(matKhau))
            {
                TxtMatKhau.Focus();
                return;
            }

            var userResult = _bus.GetLoginInfor(tenDangNhap, matKhau);
            if (userResult.Rows.Count > 0)
            {
                MessageBox.Show("Đăng nhập thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                var main=new frmMain();
                main.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Tên đăng nhập và mật khẩu không chính xác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
