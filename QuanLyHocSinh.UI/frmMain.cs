﻿using System.Windows.Forms;

namespace QuanLyHocSinh.UI
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void lớpHọcToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            var frm = new frmLopHoc();
            frm.MdiParent = this;
            frm.WindowState = FormWindowState.Maximized;
            frm.Show();
        }
    }
}
