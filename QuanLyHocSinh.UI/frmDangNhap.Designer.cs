﻿namespace QuanLyHocSinh.UI
{
    partial class frmDangNhap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDangNhap));
            this.TxtTenDangNhap = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtMatKhau = new System.Windows.Forms.TextBox();
            this.BtnThoat = new System.Windows.Forms.Button();
            this.BtnDangNhap = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TxtTenDangNhap
            // 
            this.TxtTenDangNhap.Location = new System.Drawing.Point(127, 34);
            this.TxtTenDangNhap.Margin = new System.Windows.Forms.Padding(4);
            this.TxtTenDangNhap.Name = "TxtTenDangNhap";
            this.TxtTenDangNhap.Size = new System.Drawing.Size(267, 25);
            this.TxtTenDangNhap.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tên đăng nhập";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Mật khẩu";
            // 
            // TxtMatKhau
            // 
            this.TxtMatKhau.Location = new System.Drawing.Point(127, 67);
            this.TxtMatKhau.Margin = new System.Windows.Forms.Padding(4);
            this.TxtMatKhau.MaxLength = 255;
            this.TxtMatKhau.Name = "TxtMatKhau";
            this.TxtMatKhau.PasswordChar = '*';
            this.TxtMatKhau.Size = new System.Drawing.Size(267, 25);
            this.TxtMatKhau.TabIndex = 1;
            // 
            // BtnThoat
            // 
            this.BtnThoat.Image = global::QuanLyHocSinh.UI.Properties.Resources.exit;
            this.BtnThoat.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnThoat.Location = new System.Drawing.Point(207, 118);
            this.BtnThoat.Name = "BtnThoat";
            this.BtnThoat.Size = new System.Drawing.Size(111, 32);
            this.BtnThoat.TabIndex = 3;
            this.BtnThoat.Text = "Thoát";
            this.BtnThoat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnThoat.UseVisualStyleBackColor = true;
            this.BtnThoat.Click += new System.EventHandler(this.BtnThoat_Click);
            // 
            // BtnDangNhap
            // 
            this.BtnDangNhap.Image = global::QuanLyHocSinh.UI.Properties.Resources.yes;
            this.BtnDangNhap.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnDangNhap.Location = new System.Drawing.Point(66, 118);
            this.BtnDangNhap.Name = "BtnDangNhap";
            this.BtnDangNhap.Size = new System.Drawing.Size(111, 32);
            this.BtnDangNhap.TabIndex = 2;
            this.BtnDangNhap.Text = "Đăng nhập";
            this.BtnDangNhap.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnDangNhap.UseVisualStyleBackColor = true;
            this.BtnDangNhap.Click += new System.EventHandler(this.BtnDangNhap_Click);
            // 
            // frmDangNhap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(433, 171);
            this.Controls.Add(this.TxtMatKhau);
            this.Controls.Add(this.BtnThoat);
            this.Controls.Add(this.BtnDangNhap);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtTenDangNhap);
            this.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDangNhap";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đăng nhập";
            this.TopMost = true;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmDangNhap_FormClosed);
            this.Load += new System.EventHandler(this.frmDangNhap_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxtTenDangNhap;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button BtnDangNhap;
        private System.Windows.Forms.Button BtnThoat;
        private System.Windows.Forms.TextBox TxtMatKhau;
    }
}