﻿using System;
using System.Windows.Forms;

namespace QuanLyHocSinh.UI
{
    public partial class BaseForm : Form
    {
        public BaseForm()
        {
            InitializeComponent();
        }

        protected bool IsInsert = false;

        protected virtual void HienThi() { }

        protected virtual void Lock()
        {

            BtnTaoMoi.Enabled = true;
            BtnLuu.Enabled = false;
            BtnSua.Enabled = true;
            BtnXoa.Enabled = true;
        }

        protected virtual void UnLock()
        {
            BtnTaoMoi.Enabled = false;
            BtnLuu.Enabled = true;
            BtnSua.Enabled = false;
            BtnXoa.Enabled = false;
        }

        protected virtual void Clear() { }

        protected virtual void TaoMoi()
        {
            UnLock(); Clear();
            IsInsert = true;
        }

        protected virtual void Luu() { HienThi(); Lock(); Clear(); }

        protected virtual void Sua()
        {
            UnLock();
            IsInsert = false;
        }

        protected virtual void Xoa() { HienThi(); Lock(); Clear(); }

        protected virtual void TaiLai() { HienThi(); }

        private void BtnTaoMoi_Click(object sender, EventArgs e)
        {
            TaoMoi();
        }

        private void BtnLuu_Click(object sender, EventArgs e)
        {
            Luu();
        }

        private void BtnXoa_Click(object sender, EventArgs e)
        {
            Xoa();
        }

        private void BtnTaiLai_Click(object sender, EventArgs e)
        {
            TaiLai();
        }

        private void BtnSua_Click(object sender, EventArgs e)
        {
            Sua();
        }

        private void BaseForm_Load(object sender, EventArgs e)
        {
            HienThi();
            Clear();
            Lock();
        }
    }
}
