﻿namespace QuanLyHocSinh.Common
{
    public enum Status
    {
        Deleted = 0,
        Active = 1,
        DeActive = 2
    }
}
