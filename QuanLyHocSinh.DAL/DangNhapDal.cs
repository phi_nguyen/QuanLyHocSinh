﻿using System;
using System.Data;
using QuanLyHocSinh.Common;
using QuanLyHocSinh.Model;

namespace QuanLyHocSinh.DAL
{
    public abstract class DangNhapDal : ICRUD<DangNhapModel, string>
    {
        private readonly ConnectDb _db;

        protected DangNhapDal()
        {
            _db = new ConnectDb();
        }

        public DataTable GetData(string where)
        {
            var strSelect = "Select * from DangNhap";
            if (!string.IsNullOrWhiteSpace(where))
            {
                strSelect = string.Format("{0} WHERE {1}", strSelect, where);
            }
            return _db.GetData(strSelect);
        }

        public void Delete(string id)
        {
            var str = string.Format("Update DangNhap Set TinhTrang={0} where TenDangNhap=N'{1}'", (short)Status.Deleted, id);
            _db.ExecuteSql(str);
        }

        public int Insert(DangNhapModel entitty)
        {
            string str =
               String.Format("INSERT INTO dbo.DangNhap(TenDangNhap, MatKhau, TenHienThi, ThoiGianDangNhapGanNhat, TinhTrang)VALUES (N'{0}',N'{1}',N'{2}',NULL,{3})",
                   entitty.TenDangNhap, entitty.MatKhau, entitty.TenHienThi, (short)Status.Active);
            return _db.ExecuteSql(str);
        }

        public int Update(DangNhapModel entitty)
        {
            string str = string.Format("UPDATE dbo.DangNhap SET TenHienThi=N'{0}' WHERE TenDangNhap=N'{1}'", entitty.TenHienThi, entitty.TenDangNhap);
            return _db.ExecuteSql(str);
        }
    }
}
