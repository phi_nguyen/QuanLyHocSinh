﻿using System.Data;

namespace QuanLyHocSinh.DAL
{
    public interface ICRUD<T, K>
    {
        DataTable GetData(string where);

        void Delete(K id);

        int Insert(T entitty);

        int Update(T entitty);
    }
}
