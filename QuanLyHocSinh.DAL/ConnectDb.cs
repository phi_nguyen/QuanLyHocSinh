﻿using System.Data;
using System.Data.SqlClient;

namespace QuanLyHocSinh.DAL
{
    public class ConnectDb
    {
        private readonly SqlConnection _sqlcon = new SqlConnection(@"Server=CPP00101494C\SQLEXPRESS;Database=QuanLyHocSinh;Trusted_Connection=True;");

        private void OpenConnect()
        {
            if (_sqlcon.State == ConnectionState.Closed)
                _sqlcon.Open();
        }

        private void CloseConnect()
        {
            if (_sqlcon.State != ConnectionState.Closed)
                _sqlcon.Close();
        }

        public DataTable GetData(string strSql)
        {
            OpenConnect();
            var dt = new DataTable();
            var sqlda = new SqlDataAdapter(strSql, _sqlcon);
            sqlda.Fill(dt);
            CloseConnect();
            return dt;
        }

        public int ExecuteSql(string strSql)
        {
            OpenConnect();
            var sqlCmd = new SqlCommand(strSql, _sqlcon);
            var result = sqlCmd.ExecuteNonQuery();
            CloseConnect();
            return result;
        }

    }
}
