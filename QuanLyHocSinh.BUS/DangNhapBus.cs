﻿using System.Data;
using QuanLyHocSinh.DAL;

namespace QuanLyHocSinh.BUS
{
    public class DangNhapBus : DangNhapDal
    {
        public DataTable GetLoginInfor(string userName, string passWord)
        {
            var strWhere = string.Format("TenDangNhap='{0}' And MatKhau='{1}'", userName, passWord);
            var result = GetData(strWhere);
            return result;
        }

        public bool KiemTraTenDangNhap(string userName)
        {
            var strWhere = string.Format("TenDangNhap='{0}'", userName);
            var result = GetData(strWhere);
            if (result.Rows.Count > 0)
                return true;
            return false;
            //return result.Rows.Count > 0;
        }
    }
}
